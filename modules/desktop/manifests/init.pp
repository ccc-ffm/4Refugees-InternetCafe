class desktop {

	file { '/etc/skel/Desktop':
		ensure => directory,
	}

	package { 'xinit':
		ensure => installed,
	}

	package { 'lxde':
		ensure => installed,
		require => Package['xinit'],
	}

	file { '/etc/skel/Desktop/libreoffice-startcenter.desktop':
		source => '/usr/share/applications/libreoffice-startcenter.desktop',
		require => [
			Package['libreoffice'],
			File['/etc/skel/Desktop'],
		],
	}

	file { '/etc/skel/Desktop/lxde-logout.desktop':
		source => '/usr/share/applications/lxde-logout.desktop',
		require => [
			Package['lxde'],
			File['/etc/skel/Desktop'],
		],
	}

	file { '/etc/skel/Desktop/leafpad.desktop':
		source => '/usr/share/applications/leafpad.desktop',
		require => [
			Package['lxde'],
			File['/etc/skel/Desktop'],
		],
	}

	file { '/etc/skel/Desktop/galculator.desktop':
		source => '/usr/share/applications/galculator.desktop',
		require => [
			Package['lxde'],
			File['/etc/skel/Desktop'],
		],
	}

	file { '/etc/skel/Desktop/chromium-l10n.desktop':
		source => '/usr/share/applications/chromium.desktop',
		require => [
			Package['chromium'],
			File['/etc/skel/Desktop'],
		],
	}
	
	file { '/etc/skel/Desktop/florence.desktop':
		source => '/usr/share/applications/florence.desktop',
		require => [
			Package['florence'],
			File['/etc/skel/Desktop'],
		],
	}

	$dms = [
		'lightdm*',
		'kdm',
		'gdm3',
		'nodm',
		'wdm',
		'xdm',
		'slim',
	]

	package { $dms:
		ensure => absent,
		require => Package['lxde'],
	}

	$packages = [
		'iceweasel',
		'iceweasel-l10n-*',
		'gstreamer1.0-libav',
		'gstreamer1.0-plugins-good',
		'flashplugin-nonfree',
		'chromium',
		'chromium-l10n',
		'libreoffice',
		'libreoffice-help-*',
		'libreoffice-l10n-*',
		'ttf-mscorefonts-installer',
		'ttf-liberation',
		'fonts-crosextra-carlito',
		'fonts-crosextra-caladea',
                'florence',
                'avahi-utils',
                'avahi-daemon',
                'libnss-mdns',
                'cups',
                'hplip',

	]
  
	package { $packages:
		ensure => installed,
	}

	file { '/etc/skel/Desktop/iceweasel.desktop':
		source => '/usr/share/applications/iceweasel.desktop',
		require => [
			Package['iceweasel'],
			File['/etc/skel/Desktop'],
		],
	}

	file { '/etc/iceweasel/profile/prefs.js':
		source => 'puppet:///modules/desktop/etc/iceweasel/profile/prefs.js',
		require => [
			Package['iceweasel'],
		],
	}

	group { 'guest':
		ensure => 'present'
	}

	user { 'guest':
		uid => 5000,
		home => '/run/user/5000/home',
		# password: guest
		password => '$6$pheeghohphaikuka$lJodCaTfZG4IhXw9.htxDsMDAiQrhOsXyJR.nvCdWc2EHgmstkTrpn/8LWi83e9Ac.WFDfzmQ1phpDII/npl/0',
		managehome => false,
		gid => 'guest',
		groups => [
				'plugdev',
				'cdrom',
				'audio',
				'video',
			],
		shell => '/bin/bash',
		require => Group['guest'],
		ensure => 'present',
	}

	$packages_i386 = [
		'libc6:i386',
		'libqt4-dbus:i386',
		'libqt4-network:i386',
		'libqt4-xml:i386',
		'libqtcore4:i386',
		'libqtgui4:i386',
		'libqtwebkit4:i386',
		'libstdc++6:i386',
		'libx11-6:i386',
		'libxext6:i386',
		'libxss1:i386',
		'libxv1:i386',
		'libssl1.0.0:i386',
		'libpulse0:i386',
		'libasound2-plugins:i386',
	]

	package { $packages_i386:
		ensure => installed,
		require => [
				Exec['add-architecture-i386'],
				Exec['apt_update'],
			],
	}

	exec { 'download-skype':
		command => '/usr/bin/wget -qO/var/cache/.skype-install.deb.part http://www.skype.com/go/getskype-linux-deb && /bin/mv /var/cache/.skype-install.deb.part /var/cache/skype-install.deb',
		creates => '/var/cache/skype-install.deb',
	}

	package { 'skype':
		provider => dpkg,
		ensure => latest,
		source => '/var/cache/skype-install.deb',
		require => [
			Exec['download-skype'],
			Package['libc6:i386'],
			],	
	}

	file { '/etc/skel/Desktop/skype.desktop':
		source => '/usr/share/applications/skype.desktop',
		require => [
			Package['skype'],
			File['/etc/skel/Desktop'],
		],
	}

	exec { 'enable-guestx11':
		command => '/bin/systemctl enable guestx11.service',
		refreshonly => true,
	}
		
	exec { 'start-guestx11':
		command => '/bin/systemctl start guestx11.service',
		refreshonly => true,
	}

	file { '/usr/local/sbin/x11login':
		source => 'puppet:///modules/desktop/usr/local/sbin/x11login',
		mode => 'a=rx',
		ensure => file,
	}

	file { '/etc/skel':
		source => 'puppet:///modules/desktop/etc/skel',
		recurse => true,
	}

	file { '/etc/systemd/system/guestx11.service':
		source => 'puppet:///modules/desktop/etc/systemd/system/guestx11.service',
		mode => 'a=rx',
		require => [
				Package['lxde'],
				File['/etc/skel/Desktop'],
				File['/usr/local/sbin/x11login'],
				File['/etc/skel'],
			],
		notify => [
				Exec['enable-guestx11'],
				Exec['start-guestx11'],
			],
		ensure => file,
	}
	
	printer { "mfp01":
    		ensure       => present,
    		uri          => "dnssd://mfp01._ipp._tcp.local/",
    		description  => "mfp01",
    		location     => "here",
		require	     => [ 
			Package['avahi-utils'],
                	Package['avahi-daemon'],
                        Package['libnss-mdns'],
                        Package['cups'],
                        Package['hplip'],
			],
    		model        => "drv:///hpcups.drv/hp-deskjet_990c.ppd", 
    		shared       => false, # Printer will be shared and published by CUPS
    		error_policy => abort_job, # underscored version of error policy
    		enabled      => true, # Enabled by default
    		page_size    => 'A4',

	}
}
