class system {

	class { 'apt':
		purge => {
			'sources.list' => true,
			'sources.list.d' => true,
			'preferences' => true,
			'preferences.d' => true,
		},
		sources => {
			'debian' => {
				location => 'http://ftp.de.debian.org/debian/',
				release => "${lsbdistcodename}",
				repos => 'main non-free contrib',
			},
			'debian-security' => {
				location => 'http://security.debian.org/',
				release => "${lsbdistcodename}/updates",
				repos => 'main non-free contrib',
			},
			'debian-volatile' => {
				location => 'http://ftp.de.debian.org/debian/',
				release => "${lsbdistcodename}-updates",
				repos => 'main non-free contrib',
			},
		},
		update => {
			frequency => 'always',
		},
	}
	

	exec { 'add-architecture-i386':
		command => '/usr/bin/dpkg --add-architecture i386',
		unless => '/bin/fgrep -q i386 /var/lib/dpkg/arch',
		notify => Exec['apt_update'],
	}

	exec { 'aptget-update':
		command => '/usr/bin/apt-get update',
		refreshonly => true,
	}

	$packages = [
		'sudo',
		'lsof',
		'vim',
		'screen',
		'rsync',
		'tcpdump',
		'pv',
		'openssh-server',
		'locales-all',
		'libpam-tmpdir',
		'broadcom-sta-dkms',
		'firmware-iwlwifi',
	]
  
	package { $packages:
		ensure => installed,
	}

	include pam
	include pam::mkhomedir

	file { '/etc/pam.d/common-session':
		source => 'puppet:///modules/system/etc/pam.d/common-session',
		require => Exec['pam_auth_update'],
	}

	exec { 'systemd-reload':
		command => '/bin/systemctl daemon-reload',
		refreshonly => true,
	}

	file { '/etc/systemd/logind.conf':
		source => 'puppet:///modules/system/etc/systemd/logind.conf',
		notify => Exec['systemd-reload'],
	}

	package { 'chrony':
		ensure => installed,
	}

	service { 'chrony':
		ensure => running,
		enable => true,
		hasstatus  => false,
		hasrestart => true,
		require => Package['chrony'],
	}

	package { 'network-manager':
		ensure => installed,
	}

	package { 'network-manager-gnome':
		ensure => installed,
	}

	file { '/etc/network/interfaces':
		ensure => file,
		source => 'puppet:///modules/system/etc/network/interfaces',
		mode => 'a=r',
		require => Package['network-manager'],
	}

	file { '/etc/NetworkManager/dispatcher.d/99-wlan':
		ensure => file,
		source => 'puppet:///modules/system/etc/NetworkManager/dispatcher.d/99-wlan',
		mode => 'a=rx',
		require => Package['network-manager'],
	}

	file { '/etc/NetworkManager/system-connections/ffm.freifunk.net':
		ensure => file,
		source => 'puppet:///modules/system/etc/NetworkManager/system-connections/ffm.freifunk.net',
		mode => 'a=,u=r',
		require => Package['network-manager'],
	}

	file { '/etc/NetworkManager/system-connections/wired':
		ensure => file,
		source => 'puppet:///modules/system/etc/NetworkManager/system-connections/wired',
		mode => 'a=,u=r',
		require => Package['network-manager'],
	}

	package { 'wicd*':
		ensure => absent,
		require => Package['network-manager'],
	}

}
