class persist {

	file { '/usr/local/sbin/git2puppet':
		ensure => present,
		content => "#!/bin/bash\ncd /etc/puppet && git pull && git submodule init && git submodule update && puppet apply /etc/puppet/manifests/site.pp\n",
		mode => '0755',
	}

	$packages = [
		'git-core',
		'puppet',
	]
  
	package { $packages:
		ensure => installed,
	}	

	service { 'puppet':
		enable => false,
	}

}
