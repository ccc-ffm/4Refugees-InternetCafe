#!/bin/bash -x

GITPROJ=https://chaos.expert/ccc-ffm/4Refugees-InternetCafe
ATHQ=false
HQSERVER="install.hq"
PROXYURL="http://${HQSERVER}:8080/"
if [ "$1" != "RUNME" ]; then

    #pull myself from git repo
    wget --prefer-family=IPv4 "${GITPROJ}/raw/master/bootstrap.sh" -O /root/bootstrap.sh
    chmod u+x /root/bootstrap.sh
    exec /root/bootstrap.sh RUNME
    exit
fi

#check for install.hq
if ping -c 5 ${HQSERVER}; then
    ATHQ=true
fi


if [ "$ATHQ" = true ]; then
    echo 'Acquire::http::Proxy "${PROXYURL}";' > /etc/apt/apt.conf
else
    echo "#no proxy" > /etc/apt/apt.conf
fi

apt update
apt install --assume-yes git puppet vim curl
apt full-upgrade --assume-yes


if [ ! -e /etc/puppet/.git ]; then  
    mv /etc/puppet /etc/puppet.orig
    git clone ${GITPROJ}.git /etc/puppet
fi


cd /etc/puppet
git remote set-url origin ${GITPROJ}.git
git pull
git submodule init
git submodule update
git pull
puppet apply /etc/puppet/manifests/site.pp

if [ "$ATHQ" = true ]; then
    facter --yaml --puppet > /tmp/facts.yaml
    curl -T /tmp/facts.yaml ftp://${HQSERVER}/refugee-notebooks/${HOSTNAME}.yaml
fi
